from create_structure import *
from draw import ResultDraw
from entity import Unit

from tkinter import *
from tkinter.messagebox import *


def get_structures():
    w = width.get()
    h = height.get()
    try:
        unit = Unit(int(w), int(h))
        if unit.is_valid():
            structures = create_staructures(unit)
            result_draw = ResultDraw(unit, structures)
            result_draw.draw()
        else:
            showerror('Error', 'This Unit cannot be divided into three parts with given restrictions')
    except ValueError:
        showerror('Error', 'Invalid input type.')


if __name__ == '__main__':
    master = Tk()

    Label(master, text="Unit width").grid(row=0)
    Label(master, text="Unit height").grid(row=1)

    width = Entry(master)
    height = Entry(master)

    width.grid(row=0, column=1)
    height.grid(row=1, column=1)

    Button(master, text='Quit', command=master.destroy).grid(row=3, column=0, sticky=W, pady=4)
    Button(master, text='Get structure', command=get_structures).grid(row=3, column=1, sticky=W, pady=4)

    master.mainloop()

