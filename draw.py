from tkinter import *


FRAME_SIDE = 300
INDENT = 20
BUTTON_FRAME_HEIGHT = 70
WINDOW_SIDE = FRAME_SIDE + 2 * INDENT


class ResultDraw:
    indent = 10

    def __init__(self, unit, structures):
        """
        scale_coef = draw scale
        vector - centering vector
        counter = number of showed structure
        """
        self.unit = unit
        self.structures = structures
        self.str_number = len(self.structures)

        self.counter = 0
        self.scale_coef = int(FRAME_SIDE / (max(self.unit.width, self.unit.height)))
        self.vector = ((FRAME_SIDE - self.unit.width * self.scale_coef) / 2,
                       (FRAME_SIDE - self.unit.height * self.scale_coef) / 2)

        self.graph = None
        self.canvas = None
        self.frame1 = None
        self.frame2 = None
        self.frame3 = None

    def get_scale(self, diagonal):
        return (
            self.vector[0] + diagonal[0] * self.scale_coef + INDENT,
            self.vector[1] + diagonal[1] * self.scale_coef + INDENT,
            self.vector[0] + diagonal[2] * self.scale_coef + INDENT,
            self.vector[1] + diagonal[3] * self.scale_coef + INDENT,
        )

    def draw_room(self, structure, diagonal):
        scaled_diagonal = self.get_scale(diagonal)
        self.canvas.create_rectangle(scaled_diagonal)
        position = (
            (scaled_diagonal[2] + scaled_diagonal[0]) / 2,
            (scaled_diagonal[3] + scaled_diagonal[1]) / 2,
        )
        self.canvas.create_text(position, text='S=%s' % structure.areas[diagonal])
        self.canvas.create_text(((scaled_diagonal[2] + scaled_diagonal[0]) / 2,
                                 scaled_diagonal[3]),
                                text=str(abs(diagonal[2]-diagonal[0])),
                                anchor='n')
        self.canvas.create_text((scaled_diagonal[0],
                                 (scaled_diagonal[3] + scaled_diagonal[1]) / 2
                                 ),
                                text=str(abs(diagonal[3] - diagonal[1])),
                                anchor='ne')

    def draw_structure(self, structure):
        for diagonal in structure.diagonals:
            self.draw_room(structure, diagonal)

    def next_draw(self):
        self.counter += 1
        self.create_window()

    def prev_draw(self):
        self.counter -= 1
        self.create_window()

    def create_buttons(self):
        if self.frame2:
            self.frame2.destroy()

        if self.str_number > 1:
            self.frame2 = Frame(self.graph, width=WINDOW_SIDE, height=BUTTON_FRAME_HEIGHT)
            state_previous = (self.counter < 1) and DISABLED or NORMAL
            state_next = (self.counter > self.str_number-2) and DISABLED or NORMAL
            Button(self.frame2, state=state_previous, text='Previous', command=self.prev_draw).pack()
            Button(self.frame2, state=state_next, text='Next', command=self.next_draw).pack()
            self.frame2.pack()

    def create_window(self):
        self.canvas.delete('all')
        self.graph.title('The structure %d from %d' % (self.str_number, self.counter + 1))
        self.draw_structure(self.structures[self.counter])
        self.create_buttons()

    def save(self):
        print('Saving structure %s with name %s' % (self.structures[self.counter], self.name.get()))

    def draw(self):
        self.graph = Toplevel()
        self.frame1 = Frame(self.graph, width=WINDOW_SIDE, height=WINDOW_SIDE)
        self.frame3 = Frame(self.graph, width=WINDOW_SIDE, height=BUTTON_FRAME_HEIGHT)
        self.canvas = Canvas(self.frame1, width=WINDOW_SIDE, height=WINDOW_SIDE)

        Label(self.frame3, text="Name").grid(row=0)
        self.name = Entry(self.frame3)
        self.name.grid(row=0, column=1)
        Button(self.frame3, text='Save to DB', command=self.save).grid(row=3, column=0, sticky=W, pady=4)
        self.canvas.pack()
        self.frame3.pack()
        self.frame1.pack()

        self.create_window()
