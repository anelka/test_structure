import restrictions


class Unit:
    """
    Objects of this class are the base rectangles, which we should divide
    """
    def __init__(self, width: int, height: int, name=None, price=None):
        self.width = width
        self.height = height
        self.name = name
        self.price = price

    def is_valid(self) -> bool:
        """
        Check if it possible to devide unit into three rooms with given restictions
        :return: True | FalseGet all possible structures with T-type division
        """
        area = self.width * self.height

        if area < restrictions.MIN_AREA * restrictions.ROOM_NUMBER:
            return False
        if area > restrictions.MAX_AREA * restrictions.ROOM_NUMBER:
            return False

        return True


class Structure:
    def __init__(self, unit: Unit, diagonals: list):
        self.unit = unit
        self.diagonals = diagonals
        self.areas = {d: abs((d[0]-d[2])*(d[1]-d[3])) for d in diagonals}

    def is_valid(self) -> bool:
        """ Check restrictions for the room sizes of this Structure object"""
        return all(area >= restrictions.MIN_AREA and area <= restrictions.MAX_AREA
                   for area in self.areas.values())

    def __eq__(self, other):
        return self.unit == other.unit and self.diagonals == other.diagonals
