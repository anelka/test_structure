from entity import *


def create_structures_type_T(unit: Unit) -> list:
    """ Get all possible structures with T-type division """

    result = []
    for x in range(restrictions.MIN_ROOM_WIDTH, unit.width-restrictions.MIN_ROOM_WIDTH):
        for y in range(restrictions.MIN_ROOM_WIDTH, unit.height-restrictions.MIN_ROOM_WIDTH):
            diagonals = [
                ((0, 0, x, unit.height),
                 (x, y, unit.width, unit.height),
                 (x, y, unit.width, 0)),
                ((0, 0, x, y),
                 (0, y, unit.width, unit.height),
                 (x, y, unit.width, 0))
            ]
            for struc in diagonals:
                structure = Structure(unit, struc)
                if structure.is_valid():
                    result.append(structure)
    return result


def create_structure_parallel(unit: Unit) -> list:
    """ Get all possible structures with parallel walls """

    result = []
    for x1 in range(restrictions.MIN_ROOM_WIDTH, unit.width - restrictions.MIN_ROOM_WIDTH - 1):
        for x2 in range(x1 + restrictions.MIN_ROOM_WIDTH, unit.width - restrictions.MIN_ROOM_WIDTH):
            diagonals = (
                (0, 0, x1, unit.height),
                (x1, 0, x2, unit.height),
                (x2, 0, unit.width, unit.height)
            )
            structure = Structure(unit, diagonals)
            if structure.is_valid():
                result.append(structure)

    for y1 in range(restrictions.MIN_ROOM_WIDTH, unit.height - restrictions.MIN_ROOM_WIDTH - 1):
        for y2 in range(y1 + restrictions.MIN_ROOM_WIDTH, unit.height - restrictions.MIN_ROOM_WIDTH):
            diagonals = (
                (0, 0, unit.width, y1),
                (0, y1, unit.width, y2),
                (0, y2, unit.width, unit.height)
            )
            structure = Structure(unit, diagonals)
            if structure.is_valid():
                result.append(structure)

    return result


def create_staructures(unit: Unit) -> list:
    """
    Divide Unit into three parts and return the list of possible structures.
    We have 4 types of division: T, |-, || and - -
    Suppose that lower left conner has coordinates (0, 0)
    :param unit: divided Unit object
    :return: list of possible Structures
    """
    return create_structures_type_T(unit) + create_structure_parallel(unit)
